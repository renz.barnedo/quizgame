export interface Difficulty {
  value: string;
  viewValue: string;
}
