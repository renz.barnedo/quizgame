import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class QuizService {
  API_URL: string = 'https://opentdb.com';

  constructor(private http: HttpClient) {}

  getCategories() {
    return new Promise((resolve, reject) => {
      this.http.get(`${this.API_URL}/api_category.php`).subscribe(
        (response) => resolve(response),
        (error) => reject(error)
      );
    });
  }

  getQuestions(query: any) {
    const url = this.API_URL + '/api.php' + query;
    return new Promise((resolve, reject) => {
      this.http.get(url).subscribe(
        (response) => resolve(response),
        (error) => reject(error)
      );
    });
  }
}
