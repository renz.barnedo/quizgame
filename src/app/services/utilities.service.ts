import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DialogComponent } from '../utilities/dialog/dialog.component';
import { SnackbarComponent } from '../utilities/snackbar/snackbar.component';
import { SpinnerComponent } from '../utilities/spinner/spinner.component';

@Injectable({
  providedIn: 'root',
})
export class UtilitiesService {
  constructor(private dialog: MatDialog, private snackBar: MatSnackBar) {}

  openLoadingSpinner() {
    this.dialog
      .open(SpinnerComponent, {
        disableClose: true,
        panelClass: 'transparent',
      })
      .updatePosition();
  }

  closeLoadingSpinner() {
    this.dialog.closeAll();
  }

  openDialog(data: any) {
    this.dialog.open(DialogComponent, {
      data,
      panelClass: 'dialog-template',
    });
  }

  openSnackBar(data: any) {
    this.snackBar.openFromComponent(SnackbarComponent, {
      duration: 1500,
      data: data,
      verticalPosition: 'top',
      panelClass: data.panelClass,
    });
  }
}
