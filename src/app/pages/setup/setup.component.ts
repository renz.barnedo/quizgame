import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Category } from 'src/app/models/category';
import { Difficulty } from 'src/app/models/difficulty';
import { QuizService } from 'src/app/services/quiz.service';
import { UtilitiesService } from 'src/app/services/utilities.service';

@Component({
  selector: 'app-setup',
  templateUrl: './setup.component.html',
  styleUrls: ['./setup.component.scss'],
})
export class SetupComponent implements OnInit {
  name: string = '';
  categories: Category[] = [];
  difficulties: Difficulty[] = [
    {
      value: 'any',
      viewValue: 'Any',
    },
    {
      value: 'easy',
      viewValue: 'Easy',
    },
    {
      value: 'medium',
      viewValue: 'Medium',
    },
    {
      value: 'hard',
      viewValue: 'Hard',
    },
  ];

  showForm: boolean = false;
  deferredPrompt: any;

  quizForm: FormGroup = this.fb.group({
    name: this.fb.control('', [Validators.required, Validators.maxLength(100)]),
    amount: this.fb.control(null, [
      Validators.required,
      Validators.max(20),
      Validators.min(3),
    ]),
    category: this.fb.control(null, Validators.required),
    difficulty: this.fb.control('any', Validators.required),
  });

  constructor(
    private quizService: QuizService,
    private fb: FormBuilder,
    private router: Router,
    private utilitiesService: UtilitiesService
  ) {}

  async ngOnInit() {
    try {
      window.addEventListener('beforeinstallprompt', (e) => {
        e.preventDefault();
        this.deferredPrompt = e;
      });
      window.addEventListener('appinstalled', (evt) => {
        this.utilitiesService.openDialog({
          title: 'Success',
          content: 'App install success! Check your apps list.',
        });
      });
      window.addEventListener('DOMContentLoaded', () => {
        let displayMode = 'browser tab';
        const navigatorApi: any = navigator;
        if (navigatorApi.standalone) {
          displayMode = 'standalone-ios';
        }
        if (window.matchMedia('(display-mode: standalone)').matches) {
          displayMode = 'standalone';
        }
        if (displayMode.indexOf('standalone') > -1) {
          localStorage.hasInstalledPwa = true;
          this.utilitiesService.openDialog({
            title: 'Success',
            content: 'App install success! Check your apps list.',
          });
        }
      });

      this.utilitiesService.openLoadingSpinner();
      const result: any = await this.quizService.getCategories();
      this.categories = result.trivia_categories;
      this.showForm = true;
      this.utilitiesService.closeLoadingSpinner();
    } catch (error) {
      this.utilitiesService.openDialog({
        title: 'Error',
        content: 'Cannot get categories, please reload this page.',
      });
    }
  }

  onQuizFormSubmit() {
    if (this.quizForm.status === 'INVALID') {
      this.utilitiesService.openDialog({
        title: 'Error',
        content: 'Form is invalid, wrong or missing values.',
      });
      return;
    }
    this.getQuestions(this.quizForm.value);
  }

  randomize() {
    try {
      const difficultiesIndex = this.getRandomInt(0, 3);
      const difficulty: any = this.difficulties[difficultiesIndex];

      const sortedCategories: any = this.categories.sort(this.compare);
      const maxIndex = sortedCategories.length - 1;

      const randomCategoryIndex = this.getRandomInt(0, maxIndex);
      const category = sortedCategories[randomCategoryIndex];

      const options = {
        name: 'Player 1',
        amount: this.getRandomInt(3, 20),
        category: category.id,
        difficulty: difficulty.value,
      };

      this.getQuestions(options);
    } catch (error) {
      this.utilitiesService.openDialog({
        title: 'Error',
        content: 'Cannot randomize, please try again.',
      });
    }
  }

  async getQuestions(options: any) {
    try {
      this.utilitiesService.openLoadingSpinner();

      const { name, amount, category, difficulty } = options;
      localStorage.name = JSON.stringify(name);
      const difficultyQuery =
        difficulty === 'any' ? '' : `&difficulty=${difficulty}`;
      const url = `?amount=${amount}&category=${category}${difficultyQuery}`;

      const result: any = await this.quizService.getQuestions(url);

      if (result.response_code !== 0) {
        this.randomize();
        return;
      }

      localStorage.questions = JSON.stringify(result.results);
      localStorage.quizDone = JSON.stringify(false);

      this.router.navigateByUrl('/play');
      this.utilitiesService.closeLoadingSpinner();
    } catch (error) {
      this.utilitiesService.openDialog({
        title: 'Error',
        content: 'Cannot get questions, please try again.',
      });
    }
  }

  getRandomInt(min: number, max: number) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  compare(first: any, next: any) {
    return first.id > next.id ? 1 : next.id > first.id ? -1 : 0;
  }

  installPwa() {
    if (!this.deferredPrompt) {
      this.utilitiesService.openDialog({
        title: 'Success',
        content: 'App install success! Check your apps list.',
      });
      return;
    }
    this.deferredPrompt.prompt();
    this.deferredPrompt.userChoice.then((choiceResult: any) => {
      if (choiceResult.outcome === 'accepted') {
        this.utilitiesService.openDialog({
          title: 'Success',
          content: 'App install success! Check your apps list.',
        });
      } else {
        this.utilitiesService.openDialog({
          title: 'Error',
          content: 'App Install failed! Please try again.',
        });
      }
    });
  }
}
