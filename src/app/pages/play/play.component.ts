import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UtilitiesService } from 'src/app/services/utilities.service';

@Component({
  selector: 'app-play',
  templateUrl: './play.component.html',
  styleUrls: ['./play.component.scss'],
})
export class PlayComponent implements OnInit {
  questions: any;
  quizDone: boolean = false;
  fullScoreText: any;
  width: number = window.screen.width;
  counter: any;
  timer: number = localStorage.timer ? JSON.parse(localStorage.timer) : 10;
  optionsLetter: string[] = ['A', 'B', 'C', 'D'];
  name = localStorage.name ? JSON.parse(localStorage.name) : '';
  correctSound: any;
  wrongSound: any;

  constructor(
    private router: Router,
    private utilitiesService: UtilitiesService
  ) {}

  ngOnInit(): void {
    this.correctSound = this.createSound('correct');
    this.wrongSound = this.createSound('wrong');

    let quizDone = localStorage.quizDone;
    if (quizDone) {
      quizDone = JSON.parse(localStorage.quizDone);
      if (quizDone) {
        this.router.navigateByUrl('');
        return;
      }
    }

    this.questions = localStorage.questions
      ? JSON.parse(localStorage.questions)
      : [];

    if (!this.questions.length) {
      this.router.navigateByUrl('');
    }

    const hasDoneQuestions = this.questions.find(
      (question: any) => question.isDone
    );
    if (hasDoneQuestions) {
      this.setCounter();
      return;
    }

    this.questions = this.questions.map((question: any, index: number) => {
      const options = [...question.incorrect_answers, question.correct_answer];
      question.options = this.shuffle(options);
      question.isActive = index === 0;
      question.isDone = false;
      question.playerAnswer = '';
      return question;
    });
    localStorage.questions = JSON.stringify(this.questions);

    this.setCounter();
  }

  ngOnDestroy() {
    this.destroyCounter();
  }

  destroyCounter() {
    clearInterval(this.counter);
  }

  setCounter() {
    this.counter = setInterval(() => {
      if (this.timer <= 0) {
        this.playerNoAnswer();
        return;
      }
      this.timer = this.timer - 1;
      localStorage.timer = this.timer;
    }, 1000);
  }

  resetTimer() {
    this.timer = 10;
    localStorage.timer = this.timer;
  }

  playerNoAnswer() {
    const currentQuestion = this.questions.find(
      (question: any) => question.isActive
    );
    this.playerAnswered('', currentQuestion);
    this.resetTimer();
  }

  playerAnswered(answer: string, currentQuestion: any) {
    try {
      this.questions = JSON.parse(localStorage.questions);
      this.questions = this.questions.map((question: any, index: number) => {
        if (question.question === currentQuestion.question) {
          question.isActive = false;
          question.isDone = true;
          question.playerAnswer = answer;
          question.playerIsCorrect = answer === question.correct_answer;
          this.notifyAnswer(question.playerIsCorrect, question.playerAnswer);

          if (index === this.questions.length - 1) {
            localStorage.quizDone = true;
            this.quizDone = true;
            return question;
          }
          const nextQuestion = this.questions[index + 1];
          nextQuestion.isActive = true;
        }
        return question;
      });
      localStorage.questions = JSON.stringify(this.questions);
      this.resetTimer();
      this.setResults();
    } catch (error) {
      this.utilitiesService.openDialog({
        title: 'Error',
        content: 'An error occured. Please try again.',
      });
      this.router.navigateByUrl('');
    }
  }

  createSound(type: string) {
    const doc = document;
    const audio: any = doc.createElement('audio');
    audio.src = `../../../assets/${type}.mp3`;
    audio.setAttribute('preload', 'auto');
    audio.setAttribute('controls', 'none');
    audio.style.display = 'none';
    doc.body.appendChild(audio);
    return audio;
  }

  notifyAnswer(isCorrect: boolean, answer: string) {
    if (isCorrect) {
      this.userAnswerIs('right');
      return;
    }
    if (!answer) {
      this.userAnswerIs('no');
      return;
    }
    this.userAnswerIs('wrong');
  }

  userAnswerIs(answer: any) {
    const messages: any = {
      right: 'Correct! :)',
      wrong: 'Wrong! :(',
      no: 'No answer... :|',
    };
    this.utilitiesService.openSnackBar({
      message: messages[answer],
      panelClass: [`${answer}-answer`, 'centered'],
    });
    const sounds: any = {
      right: this.correctSound,
      wrong: this.wrongSound,
      no: this.wrongSound,
    };
    sounds[answer].play();
  }

  setResults() {
    if (!this.quizDone) {
      return;
    }
    this.destroyCounter();

    const rawScore = this.questions.filter(
      (question: any) => question.playerIsCorrect
    ).length;
    const totalScore = this.questions.length;
    const percentage = ((rawScore / totalScore) * 100).toFixed(2);
    this.fullScoreText = `<strong>${rawScore}</strong> out of <strong>${totalScore}</strong> <i>(${percentage}%)</i>`;
  }

  shuffle(array: any) {
    let currentIndex = array.length,
      temporaryValue,
      randomIndex;

    while (0 !== currentIndex) {
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;

      temporaryValue = array[currentIndex];
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temporaryValue;
    }

    return array;
  }
}
